<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TagController extends Controller
{
    //
	
	 public function create(Request $request)
    {
        $tag = new Tag;
        $tag->name = 'God of War';
      

        $tag->save();

        $company = Company::find([3, 4]);
        $tag->companies()->attach($company);

        return 'Success';
    }
	public function removeCompany(Tag $tag)
{
        $company = Company::find(3);

        $tag->categories()->detach($company);
        
        return 'Success';
}
}
