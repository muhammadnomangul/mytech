<footer class="">
    <div class="col-10 offset-md-1">
        <div class="row text-center white-text">
            <div class="col-6 col-sm-3">
                <ul class="Offers">
                    <li>About</li>
                    <li>Terms & Conditions</li>
                    <li>Contact us</li>
                </ul>
            </div>
            <div class="col-6 col-sm-3">
                <ul class="Offers">
                    <li>Add Coureses</li>
                    <li>Join</li>
                    <li>Contact us</li>
                </ul>
            </div>
            <div class="col-6 col-sm-3 d-none d-sm-block">
                <ul class="Offers">
                    <li>All Courses</li>
                    <li>Online Courses</li>
                    <li>Free Courses</li>
                </ul>
            </div>
            <div class="col- col-sm-3">
                <div class="mb-2">Follow us</div>
                <ul class="SocialLinks mb-3">
                    <li>
                        <i class="fa fa-facebook"></i>
                    </li>
                    <li>
                        <i class="fa fa-twitter"></i>
                    </li>
                    <li>
                        <i class="fa fa-instagram"></i>
                    </li>
                </ul>
                <div class="font17">Copyright 2020 All Right Reserved</div>
            </div>
        </div>
    </div>
</footer>
