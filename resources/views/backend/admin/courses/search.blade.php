@extends('backend.admin.layouts.app')
@section('customSection')
    {{--    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>--}}
    {{--    <script type="text/javascript" src="//cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>--}}
    <div class="content">
        <div class="container-fluid">
            <div class="card">
                <div class="card-header card-header-primary">
                    <div class="table-title">
                        <div class="row">
                            <div class="col-sm-6">
                                <h2>Search Company</b></h2>
                            </div>
                            {{--                            <div class="col-sm-6">--}}
                            {{--                                <a href="#addCategory" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add New Category</span></a>--}}
                            {{--                                --}}{{--						<a href="#deleteEmployeeModal" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Delete</span></a>--}}
                            {{--                            </div>--}}
                             <div class="col-sm-6">
                              <div id="hide-btn" class="btn">show hide btn</div>
                             </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="show">
                      <form>
                      <div class="row">
                      <div class="col-6">
                      <div class="form-group">
                        <label>Course Name</label>
                        <input type="text" class="form-control" name="name" required>
                      </div>
                      </div>
                      <div class="col-6">
                      <div class="form-group">
                        <label>Course Name</label>
                        <input type="text" class="form-control" name="name" required>
                      </div>
                      </div>
                      </div>
                      </form>
                      <div class="row">
                        <div class="table-responsive">

                            <table class="table table-striped table-hover" id="table_id">
                                <thead>
                                <tr>
                                    {{--						<th>--}}
                                    {{--							<span class="custom-checkbox">--}}
                                    {{--								<input type="checkbox" id="selectAll">--}}
                                    {{--								<label for="selectAll"></label>--}}
                                    {{--							</span>--}}
                                    {{--						</th>--}}
                                    <th>Category</th>
                                    <th>Name</th>
                                    <th>Trainer</th>
                                    <th>Description</th>
                                    <th>Duration</th>
                                    <th>Created at</th>
                                </tr>
                                </thead>

                                <tbody>
                                @if($data)
                                    @foreach($data as $row)
                                        <tr>
                                            <td>{{$row->category->name}}</td>
                                            <td>{{$row->name}}</td>
                                            <td>{{$row->user->name}}</td>
                                            <td>{{$row->description}}</td>
                                            <td>{{$row->duration}}</td>
                                            <td>{{$row->created_at}}</td>

                                            {{--						<td>89 Chiaroscuro Rd, Portland, USA</td>--}}
                                            {{--                        <td>(171) 555-2222</td>--}}
                                            {{--                                            <td>--}}
                                            {{--                                                <a href="" data-id="{{$row->id}}" id="edit_cat"  class="edit category_edit" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i></a>--}}
                                            {{--                                                <a href="#" data-id="{{$row->id}}"  class="delete removePartner" data-toggle="modal"><i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i></a>--}}
                                            {{--                                            </td>--}}

                                        </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    {{--@section('script')--}}

@endsection
<!-- <script>
$( "#hide-btn" ).click(function() {
  $( ".show" ).toggle( "slow", function() {
    // Animation complete.
  });
}); -->
</script>
<link rel="stylesheet" href="http://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<script>

    $(document).ready(function() {
        $('#table_id').DataTable( {
            "order": [[ 2, "desc" ]]
        } );
    } );


</script>
